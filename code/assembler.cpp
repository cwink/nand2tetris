#include <algorithm>
#include <array>
#include <bitset>
#include <charconv>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>

namespace hasm {
enum class CommandType : std::uint8_t
{
  a,
  c,
  l,
  none
};

using Symbol = std::string;

using Address = std::uint16_t;

using SymbolTable = std::unordered_map<Symbol, Address>;

using Exception = std::runtime_error;

template<typename... Arguments>
[[nodiscard]] auto string_from(Arguments &&...arguments) -> std::string
{
  auto oss{std::ostringstream()};

  ((oss << std::forward<Arguments>(arguments)), ...); // NOLINT(cppcoreguidelines-pro-bounds-array-to-pointer-decay)

  return oss.str();
}

class Parser final {
  using Compute = std::tuple<const std::string_view &, const std::string_view &, const std::string_view &>;

public:
  explicit Parser(const std::string_view &filename)
  {
    this->ifs_.open(filename.data());

    if (!this->ifs_) {
      throw Exception(string_from("failed to open file '", filename, "'"));
    }
  }

  constexpr Parser() = delete;

  constexpr Parser(const Parser &) = delete;

  Parser(Parser &&) noexcept = default;

  constexpr auto operator=(const Parser &) -> Parser & = delete;

  auto operator=(Parser &&) noexcept -> Parser & = default;

  ~Parser() noexcept = default;

  constexpr auto advance() -> void
  {
    if (!this->has_more_commands_) {
      return;
    }

    for (;;) {
      this->ifs_.clear();

      std::getline(this->ifs_, this->line_);

      if (this->ifs_.eof()) {
        this->has_more_commands_ = false;

        return;
      }

      if (this->ifs_.fail()) {
        throw Exception("failed to read from file");
      }

      ++this->line_number_;

      auto sit{std::find_if_not(std::cbegin(this->line_), std::cend(this->line_), isspace)};

      if (sit == std::cend(this->line_) || *sit == '/') {
        continue;
      }

      auto eit{std::find_if_not(std::crbegin(this->line_), std::crend(this->line_), isspace).base()};

      const auto create_string_view = [this](const auto begin, const auto end) -> std::string_view {
        return {this->line_.data() +                                                          // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
                    static_cast<std::size_t>(std::distance(std::cbegin(this->line_), begin)), // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
                static_cast<std::size_t>(std::distance(begin, end))};
      };

      if (*sit == '@') {
        ++sit;

        this->symbol_.assign(sit, eit);

        this->command_type_ = CommandType::a;

        break;
      }

      if (*sit == '(' && *(eit - 1) == ')') {
        ++sit;

        --eit;

        this->symbol_.assign(sit, eit);

        this->command_type_ = CommandType::l;

        break;
      }

      const auto dit{std::find(sit, eit, '=')};

      const auto jit{std::find(sit, eit, ';')};

      auto cbit{sit};

      auto ceit{eit};

      if (dit != eit) {
        this->dest_ = create_string_view(sit, dit);

        cbit = dit + 1;
      } else {
        this->dest_ = {};
      }

      if (jit != eit) {
        this->jump_ = create_string_view(jit + 1, eit);

        ceit = jit;
      } else {
        this->jump_ = {};
      }

      this->comp_ = create_string_view(cbit, ceit);

      this->command_type_ = CommandType::c;

      break;
    }
  }

  [[nodiscard]] constexpr auto has_more_commands() const noexcept -> bool
  {
    return this->has_more_commands_;
  }

  [[nodiscard]] constexpr auto command_type() const noexcept -> CommandType
  {
    return this->command_type_;
  }

  [[nodiscard]] constexpr auto line_number() const noexcept -> std::uint64_t
  {
    return this->line_number_;
  }

  [[nodiscard]] constexpr auto symbol() const -> const std::string &
  {
    if (this->command_type_ != CommandType::a && this->command_type_ != CommandType::l) {
      throw Exception("invalid command type for retrieving symbol value");
    }

    return this->symbol_;
  }

  [[nodiscard]] constexpr auto compute() const -> Compute
  {
    if (this->command_type_ != CommandType::c) {
      throw Exception("invalid command type for retrieving compute values");
    }

    return {this->dest_, this->comp_, this->jump_};
  }

  auto reset() -> void
  {
    this->ifs_.clear();

    this->ifs_.seekg(0);

    if (this->ifs_.fail()) {
      throw Exception("failed to seek to the beginning of the file");
    }

    this->symbol_ = {};

    this->dest_ = {};

    this->comp_ = {};

    this->jump_ = {};

    this->line_number_ = 0;

    this->command_type_ = CommandType::none;

    this->has_more_commands_ = true;
  }

private:
  std::ifstream ifs_;

  std::string symbol_;

  std::string_view dest_;

  std::string_view comp_;

  std::string_view jump_;

  std::string line_;

  std::uint64_t line_number_{0};

  CommandType command_type_{CommandType::none};

  bool has_more_commands_{true};

  [[maybe_unused]] std::array<std::uint8_t, 6> padding_{0}; // NOLINT(cppcoreguidelines-avoid-magic-numbers)
};

class Computer final {
  using DestLookup = std::unordered_map<std::string_view, std::bitset<3>>;

  using CompLookup = std::unordered_map<std::string_view, std::bitset<7>>; // NOLINT(cppcoreguidelines-avoid-magic-numbers)

  using JumpLookup = std::unordered_map<std::string_view, std::bitset<3>>;

public:
  constexpr Computer() = default;

  constexpr Computer(const Computer &) = delete;

  Computer(Computer &&) noexcept = default;

  constexpr auto operator=(const Computer &) -> Computer & = delete;

  auto operator=(Computer &&) noexcept -> Computer & = default;

  ~Computer() noexcept = default;

  auto write(const Parser &parser, std::ostream &os) -> void
  {
    const auto &[dest, comp, jump] = parser.compute();

    if (this->dest_lookup_.find(dest) == std::cend(this->dest_lookup_) || this->comp_lookup_.find(comp) == std::cend(this->comp_lookup_) ||
        this->jump_lookup_.find(jump) == std::cend(this->jump_lookup_)) {
      throw Exception(string_from("invalid command '", dest, dest.empty() ? "" : "=", comp, jump.empty() ? "" : ";", jump, "' on line ", parser.line_number()));
    }

    os << "111" << this->comp_lookup_[comp] << this->dest_lookup_[dest] << this->jump_lookup_[jump] << '\n';
  }

private:
  DestLookup dest_lookup_{{{}, {0b000}},  {"M", {0b001}},  {"D", {0b010}},  {"MD", {0b011}},
                          {"A", {0b100}}, {"AM", {0b101}}, {"AD", {0b110}}, {"AMD", {0b111}}}; // NOLINT(cppcoreguidelines-avoid-magic-numbers)

  CompLookup comp_lookup_{{"0", {0b0101010}},   {"1", {0b0111111}},   {"-1", {0b0111010}},  // NOLINT(cppcoreguidelines-avoid-magic-numbers)
                          {"D", {0b0001100}},   {"A", {0b0110000}},   {"!D", {0b0001111}},  // NOLINT(cppcoreguidelines-avoid-magic-numbers)
                          {"!A", {0b0110001}},  {"-D", {0b0110000}},  {"-A", {0b0110011}},  // NOLINT(cppcoreguidelines-avoid-magic-numbers)
                          {"D+1", {0b0011111}}, {"A+1", {0b0110111}}, {"D-1", {0b0001110}}, // NOLINT(cppcoreguidelines-avoid-magic-numbers)
                          {"A-1", {0b0110010}}, {"D+A", {0b0000010}}, {"D-A", {0b0010011}}, // NOLINT(cppcoreguidelines-avoid-magic-numbers)
                          {"A-D", {0b0000111}}, {"D&A", {0b0000000}}, {"D|A", {0b0010101}}, // NOLINT(cppcoreguidelines-avoid-magic-numbers)
                          {"M", {0b1110000}},   {"!M", {0b1110001}},  {"-M", {0b1110011}},  // NOLINT(cppcoreguidelines-avoid-magic-numbers)
                          {"M+1", {0b1110111}}, {"M-1", {0b1110010}}, {"D+M", {0b1000010}}, // NOLINT(cppcoreguidelines-avoid-magic-numbers)
                          {"D-M", {0b1010011}}, {"M-D", {0b1000111}}, {"D&M", {0b1000000}}, // NOLINT(cppcoreguidelines-avoid-magic-numbers)
                          {"D|M", {0b1010101}}};                                            // NOLINT(cppcoreguidelines-avoid-magic-numbers)

  JumpLookup jump_lookup_{
      {{}, {0b000}},    {"JGT", {0b001}}, {"JEQ", {0b010}}, {"JGE", {0b011}},
      {"JLT", {0b100}}, {"JNE", {0b101}}, {"JLE", {0b110}}, {"JMP", {0b111}}, // NOLINT(cppcoreguidelines-avoid-magic-numbers)
  };
};

[[nodiscard]] inline auto create_symbol_table() -> SymbolTable
{
  return {
      {"SP", 0x0000},  {"LCL", 0x0001}, {"ARG", 0x0002}, {"THIS", 0x0003},   {"THAT", 0x0004}, {"R0", 0x0000}, {"R1", 0x0001},
      {"R2", 0x0002},  {"R3", 0x0003},  {"R4", 0x0004},  {"R5", 0x0005},     {"R6", 0x0006},   {"R7", 0x0007}, // NOLINT(cppcoreguidelines-avoid-magic-numbers)
      {"R8", 0x0008},  {"R9", 0x0009},  {"R10", 0x000A}, {"R11", 0x000B},    {"R12", 0x000C},                  // NOLINT(cppcoreguidelines-avoid-magic-numbers)
      {"R13", 0x000D}, {"R14", 0x000E}, {"R15", 0x000F}, {"SCREEN", 0x4000}, {"KBD", 0x6000}};                 // NOLINT(cppcoreguidelines-avoid-magic-numbers)
}

inline auto first_pass(Parser &parser, SymbolTable &symbol_table) -> void
{
  auto program_counter{0};

  for (parser.advance(); parser.has_more_commands(); parser.advance()) {
    switch (parser.command_type()) {
    case hasm::CommandType::a:
      [[fallthrough]];
    case hasm::CommandType::c:
      ++program_counter;

      break;
    case hasm::CommandType::l:
      symbol_table.emplace(parser.symbol(), program_counter);

      break;
    case hasm::CommandType::none:
      break;
    }
  }
}

inline auto second_pass(Parser &parser, SymbolTable &symbol_table, const std::string_view &filename) -> void
{
  auto computer{Computer{}};

  parser.reset();

  auto next_memory_location{0x10U}; // NOLINT(cppcoreguidelines-avoid-magic-numbers)

  auto ofs{std::ofstream(filename.data())};

  if (!ofs) {
      throw Exception(string_from("failed to open file '", filename, "'"));
  }

  for (parser.advance(); parser.has_more_commands(); parser.advance()) {
    switch (parser.command_type()) {
    case hasm::CommandType::a:
      if (std::all_of(std::cbegin(parser.symbol()), std::cend(parser.symbol()), isdigit)) {
        ofs << "0" << std::bitset<15>(std::stoul(parser.symbol())) << '\n'; // NOLINT(cppcoreguidelines-avoid-magic-numbers)
      } else if (symbol_table.find(parser.symbol()) != std::cend(symbol_table)) {
        ofs << "0" << std::bitset<15>(symbol_table[parser.symbol()]) << '\n'; // NOLINT(cppcoreguidelines-avoid-magic-numbers)
      } else {
        symbol_table.emplace(parser.symbol(), next_memory_location);

        ofs << "0" << std::bitset<15>(next_memory_location) << '\n'; // NOLINT(cppcoreguidelines-avoid-magic-numbers)

        ++next_memory_location;
      }

      break;
    case hasm::CommandType::c:
      computer.write(parser, ofs);

      break;
    case hasm::CommandType::l:
      [[fallthrough]];
    case hasm::CommandType::none:
      break;
    }
  }
}
} // namespace hasm

auto main(int argc, char *argv[]) -> int
{
  const auto arguments = ([](auto ac, auto av) {
    auto args{std::vector<std::string_view>{}};

    std::transform(av, av + ac, std::back_inserter(args), [](const auto arg) { return std::string_view(arg); });

    return args;
  })(argc, argv);

  if (arguments.size() < 3) {
    std::cout << "Usage: ./" << std::filesystem::path{arguments[0]}.filename().string() << " [file.asm] [file.hack]" << '\n';

    return EXIT_SUCCESS;
  }

  try {
    auto symbol_table{hasm::create_symbol_table()};

    auto parser{hasm::Parser(arguments[1])};

    hasm::first_pass(parser, symbol_table);

    hasm::second_pass(parser, symbol_table, arguments[2]);
  } catch (const std::exception &ex) {
    std::cerr << "ERROR: " << ex.what() << '\n';

    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
