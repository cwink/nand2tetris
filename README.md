# The Elements of Computing Systems Project

This repository is to store all my work and notes from following/reading the book
_The Elements of Computing Systems_ written by Noam Nisan and Shimon Schocken.

## Notes

The following are various notes on the topics covered in the book.

### Combinational Gates

_Combinational Gates_ are logic chips that perform compuation based upon combinations
of their input values. These are typically for processing (such as bitwise instructions,
and arithmetic instructions). The idea is that we can build all logic chips/components
based upon the single __NAND__ gate. This is defined as:

```
A | B | NAND(A, B)
------------------
0 | 0 | 1
0 | 1 | 1
1 | 0 | 1
1 | 1 | 0
```

With the function: `NAND(A, B)`.

The _NOT_ gate can be defined as:

```
A | NOT(A)
----------
0 | 1
1 | 0
```

An observation is that `NAND(0, 0) = 1` and `NAND(1, 1) = 0`, so that means we
have the function: `NOT(A) = NAND(A, A)`.

The _AND_ gate is defined as:

```
A | B | AND(A, B)
-----------------
0 | 0 | 0
0 | 1 | 0
1 | 0 | 0
1 | 1 | 1
```

We can see from the table that this is basically the negated version of the NAND gate
(which is obvious from the name). So we can define the function for AND
as: `AND(A, B) = NOT(NAND(A, B))`.

The _OR_ gate is defined as:

```
A | B | OR(A, B)
----------------
0 | 0 | 0
0 | 1 | 1
1 | 0 | 1
1 | 1 | 1
```

If we negate `A` and `B`, then pass it through the NAND gate, we will get the same results as
the OR table above:

```
A | B | NOT(A) | NOT(B) | NAND(NOT(A), NOT(B))
----------------------------------------------
0 | 0 | 1      | 1      | 0
0 | 1 | 1      | 0      | 1
1 | 0 | 0      | 1      | 1
1 | 1 | 0      | 0      | 1
```

This mean for the OR gate we have the function: `OR(A, B) = NAND(NOT(A), NOT(B))`.

The _XOR_ gate is defined as:

```
A | B | XOR(A, B)
-----------------
0 | 0 | 0
0 | 1 | 1
1 | 0 | 1
1 | 1 | 0
```

Using a little intuition we know that the following figures for an AND gate will create
the same results:

```
A | B | AND(A, B)
-----------------
0 | 1 | 0
1 | 1 | 1
1 | 1 | 1
1 | 0 | 0
```

Looking at how the OR gate was built above, we can obtain `A` using it and processing it
through the AND gate will give us our XOR gate values (we already know that `B` is the results
for NAND):

```
A | B | OR(A, B) | NAND(A, B) | AND(OR(A, B), NAND(A, B)
--------------------------------------------------------
0 | 0 | 0        | 1          | 0
0 | 1 | 1        | 1          | 1
1 | 0 | 1        | 1          | 1
1 | 1 | 1        | 0          | 0
```

This means for XOR we have the function: `XOR(A, B) = AND(OR(A, B), NAND(A, B))`.

The _MUX_ gate (select gate) is defined as:

```
A | B | SEL | MUX
-----------------
0 | 0 | 0   | 0
0 | 1 | 0   | 0
1 | 0 | 0   | 1
1 | 1 | 0   | 1
0 | 0 | 1   | 0
0 | 1 | 1   | 1
1 | 0 | 1   | 0
1 | 1 | 1   | 1
```

Here we need to somehow relate `SEL` to `A` and `B` seperatly. I decided since `0` meant we used
`A` and `1` meant we used `B`, I could try using an AND or an OR gate with both `A` and `B`, using
the negated version of `SEL` (since the opposite of select means using `B`) for `B`. Doing this,
it seemed that for `SEL` of `0`, input `A` gets neutralized with all 1's. The same goes for
a `SEL` of `1` for input `B`. I then noticed that we could use AND on both `OR(A, SEL)` and
`OR(B, NOT(SEL))` to get MUX:

```
A | B | SEL | NOT(SEL) | OR(A, SEL) | OR(B, NOT(SEL)) | AND(OR(A, SEL), OR(B, NOT(SEL)))
----------------------------------------------------------------------------------------
0 | 0 | 0   | 1        | 0          | 1               | 0
0 | 1 | 0   | 1        | 0          | 1               | 0
1 | 0 | 0   | 1        | 1          | 1               | 1
1 | 1 | 0   | 1        | 1          | 1               | 1
0 | 0 | 1   | 0        | 1          | 0               | 0
0 | 1 | 1   | 0        | 1          | 1               | 1
1 | 0 | 1   | 0        | 1          | 0               | 0
1 | 1 | 1   | 0        | 1          | 1               | 1
```

This means we have the function for MUX: `MUX(A, B, SEL) = AND(OR(A, SEL), OR(B, NOT(SEL)))`.

The _DEMUX_ gate is defined as follows:

```
IN | SEL | A | B
----------------
0  | 0   | 0 | 0
1  | 0   | 1 | 0
0  | 1   | 0 | 0
1  | 1   | 0 | 1
```

This gate is a little different from the others since it outputs 2 values instead of 1 value. Here
`IN` and `SEL` are our inputs, and `A` and `B` are our outputs. Essentially, `A` is `IN` if `SEL` is `0`,
and `B` is `IN` if `SEL` is `1`. Using the same kind of pattern from the _MUX_ gate, we get the following:

```
IN | SEL | NOT(SEL) | AND(IN, NOT(SEL)) | AND(IN, SEL)
------------------------------------------------------
0  | 0   | 1        | 0                 | 0
1  | 0   | 1        | 1                 | 0
0  | 1   | 0        | 0                 | 0
1  | 1   | 0        | 0                 | 1
```

This means that the _DEMUX_ function is: `A = AND(IN, NOT(SEL)), B = AND(IN, SEL)`.

#### Multi-Bit Gates

For the logic gates above, they work with one dimensional input. Typically in computers we work with
multiple bits using busses in order to provide multiple inputs (i.e. an 8-bit computer would use 8
input pins) to gates. With logic gates, we simply match the base input with the index of our pins.
An example of a 4-bit bus for a _MUX_ gate would be:

```
OUT[0] = MUX(A[0], B[0], SEL)

OUT[1] = MUX(A[1], B[1], SEL)

OUT[2] = MUX(A[2], B[2], SEL)

OUT[3] = MUX(A[3], B[3], SEL)
```

Here the index's can be considered the _pin_ numbers. This component is essential for processing
data, i.e. instructions and their inputs.

#### Multi-Way Gates

Similar to Multi-Bit gates, we have what is called Multi-Way gates. The difference between the two
is that Multi-Bit gates performs the operation on each bit that is provided. An example would be that
a Multi-Bit _OR_ gate would perform an `OR` on each pin given as input and provide the same number of
outputs. A Multi-Way _OR_ gate would take in multiple inputs, but process the inputs as a whole instead
of a sort of _loop_ over the bits like a Multi-Bit gate would do. In the case of a Multi-Way OR gate,
we _OR_ each input together and output one single bit that would tell if any of the inputs were a `1`. In
other words, a 4-bit Multi-Bit _OR_ would look like this:

```
OUT[0] = OR(A[0], B[0])

OUT[1] = OR(A[0], B[0])

OUT[2] = OR(A[0], B[0])

OUT[3] = OR(A[0], B[0])
```

Where a Multi-4Way _OR_ would look like this:

```
OUT = OR(OR(OR(IN[0], IN[1]), IN[2]), IN[3])
```

### ALU

The __ALU__ (_Arithmetic Logical Unit_) is required in CPU's to perform logical processing for
arithmetic operations. Since it is based upon processing a combination of inputs, it is considered
to be in the _combinational logic_ category. _Hack_ ALU performs the following operations:

* Addition
* Bitwise AND

The inputs for the chip are as follows:

* x0-x15
  * This is the `x` input for the chip.
* y0-y15
  * This is the `y` input for the chip.
* zx
  * Set to `1` to zero all of the `x` input before performing arithmetic.
* nx
  * Set to `1` to NOT all of the `x` input before performing arithmetic.
* ny
  * Set to `1` to NOT all of the `y` input before performing arithmetic.
* zy
  * Set to `1` to zero all of the `y` input before performing arithmetic.
* f
  * Set to `1` to perform addition, set to `0` to perform a bitwise AND.
* no
  * Set to `1` to negate the output.

The outputs for the chip are as follows:

* out
  * 16-bit value resulting from the arithmetic of this chip.
* zr
  * Is set to `1` if the `out` is a base10 `0`.
* ng
  * Is set to `1` if the `out` is a base10 negative value.

Note that `zx`, `nx`, `ny`, and `zy` are processed in order and fed into each other, i.e.:

```
ZX(x, v) = { 0000000000000000   if v = 1
           { x                  otherwise

NX(x, v) = { NOT(x)             if v = 1
           { x                  otherwise

ZY(y, v) = { 0000000000000000   if v = 1
           { y                  otherwise

NY(y, v) = { NOT(y)             if v = 1
           { y                  otherwise

xx = ZX(x, zx)

x = NX(xx, nx)

yy = ZY(y, zy)

y = NY(yy, ny)
```

#### Signed Numbers

To handle signed numbers in a binary fasion, we use the _2's comliment_ format. This is the most standard
way of handling signed numbers. The algorithm follows as:

```
         { x           if x >= 0
y(x,n) = { 2^n - x     if x < 0
         { undefined   if x >= 2^n or x < -(2^n)
```

or it can also be written as:

```
         { x           if x >= 0
y(x,n) = { 2^n + |x|   if x < 0
         { undefined   if x >= 2^n or x < -(2^n)
```

Using this equation we can calculate the following values for a 4-bit string:

```
n |  2^n |  x  | y(x,n) | base2
-------------------------------
4 |    8 | -8  |      8 |  1000
4 |    8 | -7  |     15 |  1111
4 |    8 | -6  |     14 |  1110
4 |    8 | -5  |     13 |  1101
4 |    8 | -4  |     12 |  1100
4 |    8 | -3  |     11 |  1011
4 |    8 | -2  |     10 |  1010
4 |    8 | -1  |      9 |  1001
4 |    8 |  0  |      0 |  0000
4 |    8 |  1  |      1 |  0001
4 |    8 |  2  |      2 |  0010
4 |    8 |  3  |      3 |  0011
4 |    8 |  4  |      4 |  0100
4 |    8 |  5  |      5 |  0101
4 |    8 |  6  |      6 |  0110
4 |    8 |  7  |      7 |  0111
```

Notice that our max range is `-8` to `7`. This is because, as we can see, the most significant bit is `0` for
positive values, and `1` for negative values. So our range is officially `2^n - 1` to `-(2^n)` inclusivly.

#### Chips

The ALU (at least for the _Hack_ machine) utilizes the following chips:

* Mux16
* Not16
* And16
* Or8Way
* Add16

All except _Add16_ are specified _Logic Gates_ section above. To build this chip, we need 2 new chips, _HalfAdder_
and _FullAdder_. The HalfAdder chip performs a simple 2-bit addition, outputting both a _sum_ and _carry_.
This chip is defined as:

```
A | B | SUM | CARRY
-------------------
0 | 0 | 0   | 0
0 | 1 | 1   | 0
1 | 0 | 1   | 0
1 | 1 | 0   | 1
```

Through observation we can see that `SUM` is easily an XOR gate and that `CARRY` is an AND gate. Thus our chip
is built as:

```
SUM = XOR(A, B)

CARRY = AND(A, B)
```

The next chip is the FullAdder chip, which is similar to the HalfAdder, except it takes in another bit. This is
useful for when we add numbers that contain a _carry_ flag:

```
A | B | C | SUM | CARRY
-----------------------
0 | 0 | 0 | 0   | 0
0 | 0 | 1 | 1   | 0
0 | 1 | 0 | 1   | 0
0 | 1 | 1 | 0   | 1
1 | 0 | 0 | 1   | 0
1 | 0 | 1 | 0   | 1
1 | 1 | 0 | 0   | 1
1 | 1 | 1 | 1   | 1
```

This chip builds off the HalfAdder chip by summing `B` and `C`, then summing `A` and `HALFADDER(B, C)`, then `OR`ing
the carry flags, thus it can be defined as:

```
SUM_BC, CARRY_BC = HALFADDER(B, C)

SUM, CARRY_ABC = HALFADDER(A, SUM_BC)

CARRY = OR(CARRY_BC, CARRY_ABC)
```

The Add16 chip can then be built easily using the FullAdder chip. This is because, like mentioned before, we can feed
the _carry_ flag into the FullAdder chip as we add the consecutive bits, which gives us the defination:

```
SUM_0, CARRY_1 = HALFADDER(A0, B0)

SUM_1, CARRY_2 = FULLADDER(CARRY_1, A1, B1)

SUM_2, CARRY_3 = FULLADDER(CARRY_2, A2, B2)

...

SUM_14, CARRY_15 = FULLADDER(CARRY_14, A14, B14)

SUM_15, CARRY_16 = FULLADDER(CARRY_15, A15, B15)
```

A chip that I didn't use in my ALU implentation was the _Inc16_ chip. This chip was simply to add `1` to whatever 16-bit input
was provided. It essentially is just the _Add16_ chip, with `B` set to `0000000000000001`.

### Sequential Gates

_Sequential Gates_ are logical chips that work sequentially. In other words, they are time sensetive chips. These
are mainly used for storing bits of data. In order to syncronize the (sequential) chips, we used a _clock_ which
is an _oscillator_ that moves between `1` and `0` over time, i.e. a simplified example over _milliseconds_:

```
Oscillator | Milliseconds
-------------------------
0          | 0
1          | 1
0          | 2
1          | 3
0          | 4
1          | 5
```

The book doesn't go over the internals of the _DFF_ gate which it uses as its basis for register/memory storage. We'll work this
out ourselves here. Before building the DFF gate, you first need to understand the _SR Latch_ gate, which is used in a DFF gate.
The _latch_ gate takes two inputs, `S` for _set_, and `R` for _reset_. The purpose of a _latch_ gate is to retain the previous
value that was set or reset. The latch gate uses a feedback system that feeds the _set_ path back into the _reset_ path and vice
versa. The feedback system ensures that when the gate is in the _set_ state, it stays set, and if it's in the _reset_ state, it
stays reset. This conservation of state is called _latching_. The truth table for this is:

```
S | R | Q | NQ
--------------
0 | 0 | Q | NQ
0 | 1 | 0 | 1
1 | 0 | 1 | 0
1 | 1 | I | I
```

One thing to note is that when `S` is `1` and `R` is `1`, then the state is __invalid__. Both these inputs should never be `1`
at the same time. In the spirite of using _NAND_ gates for everything, we'll define the logic for this gate as:

```
SRLATCH(NS, NR) = { Q: NAND(NS, NQ), NAND(Q, NR) }
```

Notice that we use `NS` and `NR` instead of `S` and `R`. These are the negated version of the input required since we're using a
NAND gate instead of a NOR gate.

Now the problem with the latch gate here is that `NS` and `NR` can not be `0` at the same time as this is an invalid state. This
is an issue as many times we won't supply input to these pins (they may be `0`). We can solve this with with setting the `NR` pin
to always be the negated version of `NS`. This also makes the ciruit a litte more simpler as `NS` will always mean that `Q` is `1`
if `NS` is 0, and vice versa. Now doing this turns the circuit into a _flip-flop_, meaning it just keeps toggling the output back
and forth. This will make the chip a little useless as it won't really have the _latch_ functionality anymore. We can reconcile
this by adding a new input for a _clock_ that, when active, will trigger the set/reset mechanism, and when not active will place
the state into latch. Thus, we can define a DFF gate as:

```
DFF(D, C) = SRLATCH(NAND(D, C), NAND(C, NOT(D)))
```

This now leads us to continue on with the next gate in the book, the _binary cell_. This will be a 1-bit register that contains
the state of a single value. It contains a _data_ input, a _load_ input, a _clock_ input, and an output. This chip is basically
a combination of a MUX gate and a DFF gate. The MUX gate is included to allow a signal to change the value in the chip. The
data input is fed into the MUX gate along with the output (`Q`) of the DFF gate. So the output of this chip will always be the
set value until load is set to `1`, in which it'll take whatever value is in the data input. Also being a sequential chip it will
take in a clock signal as well. This chip can be defined as:

```
BIT(D, C, L) = DFF(MUX(D, Q, L), C)
```

Of course a binary cell isn't that useful; however, we can turn this into a _multi-bit_ gate to provide storage for an _n_-bit value,
i.e. for a 8-bit value, we would have:

```
BYTE(D0, D1, ... , D6, D7, C, L) = { BIT(D0, C, L), BIT(D1, C, L), ..., BIT(D6, C, L), BIT(D7, C, L) }
```

With all of these, we now officially have a _register_.

For _RAM_, we build this out of an array of registers. The inputs would be the 16-bit _data_, the single bit _load_, and the 3-bit _address_.
The idea is that you take the address, plug it into the chip along with the data to store (if you're storing data), and the load value to indicate
if this is a store or read operation. The trick with this is knowing how to use the address to connect to the exact register needed. The first
part is to use a DMUX8WAY chip that will take in the load value as its _input_ value, the address as its _select_ value, and then output the 8
values to different pins. If you remember from the notes above about the 8 way DMUX chip, it will output the values based upon the provided
input. This will gives us an array of bits that will indicate the proper register to write data to, i.e.:

```
DMUX8WAY(1, 000) = { 1, 0, 0, 0, 0, 0, 0, 0 }

DMUX8WAY(1, 001) = { 0, 1, 0, 0, 0, 0, 0, 0 }

DMUX8WAY(1, 010) = { 0, 0, 1, 0, 0, 0, 0, 0 }

DMUX8WAY(1, 011) = { 0, 0, 0, 1, 0, 0, 0, 0 }
```

This means we can feed each output value into the appropriate _load_ value of the register in order so we know which chip is getting written to.
The next part is to obtain the proper output. We use the the chip, MUX8WAY16, to select the proper output based upon the address.
Again, for the MUX8WAY16 chip, it will take in 8 inputs (a 16-bit value per input), and select the proper one from the given address. To provide
and example:

```
MUX8WAY16(0000, 1111, 1001, 1010, 1011, 1111, 1001, 1110, 0000) = 0000

MUX8WAY16(0000, 1111, 1001, 1010, 1011, 1111, 1001, 1110, 0001) = 1111

MUX8WAY16(0000, 1111, 1001, 1010, 1011, 1111, 1001, 1110, 0010) = 1001

MUX8WAY16(0000, 1111, 1001, 1010, 1011, 1111, 1001, 1110, 0011) = 1010
```

Using both these chips, and a register chip, we can create a proper RAM chip with 8 16-bit addresses as:

```
LDD = DMUX8WAY(LD, ADDR)

REGISTER(IN, LDD0, O0);

REGISTER(IN, LDD1, O1);

REGISTER(IN, LDD2, O2);

REGISTER(IN, LDD3, O3);

REGISTER(IN, LDD4, O4);

REGISTER(IN, LDD5, O5);

REGISTER(IN, LDD6, O6);

REGISTER(IN, LDD7, O7);

REGISTER(IN, LDD8, O8);

OUT = MUX8WAY16(O0, O1, O2, O3, O4, O5, O6, O7, ADDR)
```

To create larger RAM sizes (such as a RAM chip with 64 16-bit addresses), we apply the same process, splitting the address in two, the first
part to access the array of RAM8 chips, and the second part to access the array of registers in the selected RAM8 chip. This is much like a
recursive process. Note that the first part will always be 3-bits here as we'll always be using 8 RAM chips for our array. The second
part is fed to the lower RAM chip as its address. Here is the definition of a RAM64 chip:

```
LDD = DMUX8WAY(LD, ADDR[0-2])

RAM8(IN, LDD0, ADDR[3-5], O0);

RAM8(IN, LDD1, ADDR[3-5], O1);

RAM8(IN, LDD2, ADDR[3-5], O2);

RAM8(IN, LDD3, ADDR[3-5], O3);

RAM8(IN, LDD4, ADDR[3-5], O4);

RAM8(IN, LDD5, ADDR[3-5], O5);

RAM8(IN, LDD6, ADDR[3-5], O6);

RAM8(IN, LDD7, ADDR[3-5], O7);

OUT = MUX8WAY16(O0, O1, O2, O3, O4, O5, O6, O7, ADDR[0-2])
```

and for a RAM512 chip:

```
LDD = DMUX8WAY(LD, ADDR[0-2])

RAM64(IN, LDD0, ADDR[3-8], O0);

RAM64(IN, LDD1, ADDR[3-8], O1);

RAM64(IN, LDD2, ADDR[3-8], O2);

RAM64(IN, LDD3, ADDR[3-8], O3);

RAM64(IN, LDD4, ADDR[3-8], O4);

RAM64(IN, LDD5, ADDR[3-8], O5);

RAM64(IN, LDD6, ADDR[3-8], O6);

RAM64(IN, LDD7, ADDR[3-8], O7);

OUT = MUX8WAY16(O0, O1, O2, O3, O4, O5, O6, O7, ADDR[0-2])
```

Now another chip we have is the _program counter_. This is used to keep track of where you are in memory (address). The operation of this
chip is based around an _input_ address, a _load_ bit, an _increment_ bit, and a _reset_ bit. The input address is what you would want the PC
set to if you are loading an address into it. The load bit is what you use to tell the chip to set the address to the given input address,
and output the current PC address. The increment bit tells the chip to increment the counter internally, but output what the current PC address is.
The reset tells the chip to set the address back to `0` and output the current PC address. As you can see from the detailed description here,
the chip always outputs the current address after performing an operation (or when not performing an operation). All operations are done in
place, and the new values are obtained on the next read from the PC.

The operation is basically one conditional statement that is in the order of:

```
if reset then
  set the register to 0 and output the current register value
else if load then
  set the register to the input address and output the current register value
else if increment then
  increment the register by 1 and output the current register value
else
  output the current register value
```

The logic for this chip would then be:

```
IOUT = ADD16(ROUT, 00000001)

ILOAD = MUX(LOAD, 1, INC)

LOUT = MUX16(IOUT, IN, LOAD)

LILOAD = MUX(ILOAD, LOAD, LOAD)

FOUT = MUX16(LOUT, 00000000, RESET)

RLILOAD = MUX(LILOAD, 1, RESET)

ROUT, OUT = REGISTER(FOUT, RLILOAD)
```

As seen above, in order to acheive the conditional statements, we work our way from bottom to top. In other words, we start with our current
address, then we test if we want to increment it and do so if we should, then we check if we should load and set if we should, then we reset everything
if we should reset the register.

## Computer

The computer we're building is the _Hack_ machine. It is a very simple machine that models modern computers in a simplistic way. This machine
has the following properties:

* A 32K ROM that stores the program (instructions) sequentially.
* A _program counter_ (`PC`) that indicates the position in the 32K ROM for the next instruction to process.
* Two registers `A` and `D`.
  * `D` is a general purpose _data_ register used to store data values.
  * `A` is a general purpose _address_ register that is used to store addresses in memory for access, and may also be used to store data
    (carefully).
* Memory (runtime).
  * The first `16K` addresses are general purpose _data_ memory that stores values. This part of the chip is referenced as `M`, which is syntactic
    sugar for the _dereference_ of the `A` registers (so the `A` register has an address, and `M` is used to store or obtain the value at that address).
  * Address `0x4000` through `0x5FFF` is the `SCREEN` memory.
  * Address `0x6000` is the `KEYBOARD` memory.

### CPU

The _CPU_ is the brain of the machine that is used to manage the actual processes performed in the computer. It does the following:

* Store/read values to/from registers `A` and `D`.
* Store/read values to/from the ROM (`M`).
* Perform tasks utilitizing the _ALU_.
* Manage the program counter.

### Instructions

Instructions tell the CPU what task to perform. They are _decoded_ and the values that are decoded get passed to the specific chips inside the _CPU_ to
perform whatever operation needs to be performed. The format of the instructions is as follows:

```
Instruction | Ignored | Select | Compute           | Data       | Jump
------------------------------------------------------------
i           | x x     | a      | c1 c2 c3 c4 c5 c6 | d1 d2 d3   | j1 j2 j3
```

Here are the details of each field:

* Instruction
  * Indicates if this instruction is an _address_ instruction (`0`), or a _compute_ instruction (`1`). If it's an address instruction, then the instruction
    is taken as a constant that gets stored into the address register `A` (in a _Hack_ program, this is triggerd by prefixing a variable or constant with `@`).
    If it's a compute instruction, then it's used to perform an action inside the processor.
* Ignored
  * These bits are ignored.
* Select
  * This bit indicates if we should perform actions with the `D` and `A` register, or with the `D` and `M` values (remember, dereference of `A`).
* Compute
  * These bits indicate to the ALU what tasks to perform on the given register/ram values.
* Data
  * These bits indicate where the results from the ALU are stored.
* Jump
  * These bits indicate what kind of _jump_ should be made (if one should be made at all).

When the instruction is a _compute_ instruction, the following table is used to indicate
the operation to perform, the value to store to (if we do store), and the address to jump
to (if we do jump):

```
a | c1 | c2 | c3 | c4 | c5 | c6 | Mnemonic
------------------------------------------
0 | 1  | 0  | 1  | 0  | 1  | 0  | 0
0 | 1  | 1  | 1  | 1  | 1  | 1  | 1
0 | 1  | 1  | 1  | 0  | 1  | 0  | -1
0 | 0  | 0  | 1  | 1  | 0  | 0  | D
0 | 1  | 1  | 0  | 0  | 0  | 0  | A
0 | 0  | 0  | 1  | 1  | 0  | 1  | !D
0 | 1  | 1  | 0  | 0  | 0  | 1  | !A
0 | 0  | 0  | 1  | 1  | 1  | 1  | -D
0 | 1  | 1  | 0  | 0  | 1  | 1  | -A
0 | 0  | 1  | 1  | 1  | 1  | 1  | D+1
0 | 1  | 1  | 0  | 1  | 1  | 1  | A+1
0 | 0  | 0  | 1  | 1  | 1  | 0  | D-1
0 | 1  | 1  | 0  | 0  | 1  | 0  | A-1
0 | 0  | 0  | 0  | 0  | 1  | 0  | D+A
0 | 0  | 1  | 0  | 0  | 1  | 1  | D-A
0 | 0  | 0  | 0  | 1  | 1  | 1  | A-D
0 | 0  | 0  | 0  | 0  | 0  | 0  | D&A
0 | 0  | 1  | 0  | 1  | 0  | 1  | D|A
1 | 1  | 0  | 1  | 0  | 1  | 0  | 
1 | 1  | 1  | 1  | 1  | 1  | 1  | 
1 | 1  | 1  | 1  | 0  | 1  | 0  | 
1 | 0  | 0  | 1  | 1  | 0  | 0  | 
1 | 1  | 1  | 0  | 0  | 0  | 0  | M
1 | 0  | 0  | 1  | 1  | 0  | 1  | 
1 | 1  | 1  | 0  | 0  | 0  | 1  | !M
1 | 0  | 0  | 1  | 1  | 1  | 1  | 
1 | 1  | 1  | 0  | 0  | 1  | 1  | -M
1 | 0  | 1  | 1  | 1  | 1  | 1  | 
1 | 1  | 1  | 0  | 1  | 1  | 1  | M+1
1 | 0  | 0  | 1  | 1  | 1  | 0  | 
1 | 1  | 1  | 0  | 0  | 1  | 0  | M-1
1 | 0  | 0  | 0  | 0  | 1  | 0  | D+M
1 | 0  | 1  | 0  | 0  | 1  | 1  | D-M
1 | 0  | 0  | 0  | 1  | 1  | 1  | M-D
1 | 0  | 0  | 0  | 0  | 0  | 0  | D&M
1 | 0  | 1  | 0  | 1  | 0  | 1  | D|M
```

For the _data_ store, the following bits indicate where to store the data (if we do):

```
d1 | d2 | d3 | Mnemonic | Destination
---------------------------------------------------------------
0  | 0  | 0  | null     | Nowhere
0  | 0  | 1  | M        | Memory[A]
0  | 1  | 0  | D        | D Register
0  | 1  | 1  | MD       | Memory[A] and D register
1  | 0  | 0  | A        | A register
1  | 0  | 1  | AM       | A register and Memory[A]
1  | 1  | 0  | AD       | A register and D register
1  | 1  | 1  | AMD      | A register, Memory[A], and D register
```

For the _jump_ instruction, the following bits indicate where to jump (if we do) and
the condition for it:

```
j1 (out < 0) | j2 (out == 0) | j3 (out > 0) | Mnemonic | Effect
-------------------------------------------------------------------------------
0            | 0             | 0            | null     | No jump
0            | 0             | 1            | JGT      | If out > 0, then jump, else no jump.
0            | 1             | 0            | JEQ      | If out == 0, then jump, else no jump.
0            | 1             | 1            | JGE      | If out >= 0, then jump, else no jump.
1            | 0             | 0            | JLT      | If out < 0, then jump, else no jump.
1            | 0             | 1            | JNE      | If out != 0, then jump, else no jump.
1            | 1             | 0            | JLE      | If out <= 0, then jump, else no jump.
1            | 1             | 1            | JMP      | Jump
```

Note that here `out` indicates the result from the _ALU_ computation.

### Screen 

The hack machine has one display that is _memory mapped_ to RAM. The address for it starts at `0x4000`
and ends at `0x5FFF`. Each value is either `1` for _black_, and `0` for _white_ (in bits). It is treated
as an _array_ of values for a display that is `512` columns (`x`) and `256` rows (`y`). The `512` columns is
represented in memory as `32` 16-bit words. As a result, this memory totals 8 kilobytes. To access a
specific _pixel_ in this array, we would use the following functions:

```
GET(X, Y) = RAM[0x4000 + (Y * 32) + (X / 16)]

TOGGLE(X, Y) = { RAM[0x4000 + (Y * 32) + (X / 16)] = XOR(GET(X, Y), 2^X) }

SET(X, Y) = { IF GET(X, Y) & 2^X != 2^X THEN TOGGLE(X, Y) }

UNSET(X, Y) = { IF GET(X, Y) & 2^X == 2^X THEN TOGGLE(X, Y) }
```

In `RAM`, the `X` value is divided by `16` to get the specific _word_ associated with that section for `X`.

### Keyboard

The hack machine has a keyboard that is _memory mapped_ to RAM. The address for it is at `0x6000`. It is a
16-bit word that comprised of a code between `128` and `152` (base 10). The chart below shows what keys the
codes are mapped to:

```
Key         | Code
---------------------
Newline     | 128
Backspace   | 129
Left Arrow  | 130
Up Arrow    | 131
Right Arrow | 132
Down Arrow  | 133
Home        | 134
End         | 135
Page Up     | 136
Page Down   | 137
Insert      | 138
Delete      | 139
Escape      | 140
F1-F12      | 141-152
```

